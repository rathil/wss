package src

import (
	"github.com/gofiber/fiber/v2"
)

func (a *server) putEvent(c *fiber.Ctx) error {
	return a.subscription.putEvent(c.Query("id"), c.Body())
}
