package src

func serverGet(
	subscription *modelSubscription,
) *server {
	return &server{
		subscription: subscription,
	}
}

type server struct {
	subscription *modelSubscription
}
