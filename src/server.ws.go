package src

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gofiber/websocket/v2"
	"io"
	"log"
)

func (a *server) WS(conn *websocket.Conn) {
	chErrorRead := make(chan error, 1)
	chErrorWrite := make(chan error, 1)
	chMsg := make(listener, 1)
	defer close(chMsg)
	s := a.subscription.subscriberFactory(chMsg)
	defer s.unsubscribe()
	go func() {
		defer close(chErrorRead)
		for {
			_, msg, err := conn.ReadMessage()
			if err != nil {
				chErrorRead <- err
				return
			}
			fmt.Println("MESSAGE =", string(msg)) // TODO rathil deb remove!
			model := modelClientRequest{}
			if err := json.Unmarshal(msg, &model); err != nil {
				chErrorRead <- err
				return
			}
			if err := model.Process(s); err != nil {
				chErrorRead <- err
				return
			}
		}
	}()
	go func() {
		defer close(chErrorWrite)
		for {
			select {
			case msg := <-chMsg:
				if msg == nil {
					return
				}
				if err := conn.WriteMessage(websocket.TextMessage, msg); err != nil {
					chErrorWrite <- err
					return
				}
			}
		}
	}()
	var err error
	select {
	case err = <-chErrorRead:
	case err = <-chErrorWrite:
	case err = <-s.end():
	}
	if err != nil && !errors.Is(err, io.EOF) {
		log.Printf("error: %s", err)
	}
}
