package src

import (
	"github.com/urfave/cli/v2"
	"time"
)

func configGet(
	cCli *cli.Context,
) config {
	return config{
		connectionGc:      cCli.Duration("connection-gc"),
		connectionTimeout: cCli.Duration("connection-timeout"),
		port:              cCli.Int("port"),
	}
}

type config struct {
	connectionGc      time.Duration
	connectionTimeout time.Duration
	port              int
}
