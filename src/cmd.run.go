package src

import (
	"context"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/rdi.git"
)

func CmdRun(
	cCli *cli.Context,
) error {
	return rdi.New().
		MustProvide(cCli).
		MustProvide(func() context.Context {
			return cCli.Context
		}, rdi.ProviderOptionSingleInstance()).
		MustProvide(configGet, rdi.ProviderOptionSingleInstance()).
		MustProvide(serverGet, rdi.ProviderOptionSingleInstance()).
		MustProvide(modelSubscriptionGet).
		Invoke(func(
			di *rdi.DI,
			conf config,
			server *server,
		) error {
			app := fiber.New(fiber.Config{
				DisableStartupMessage: true,
			})
			app.Use("/ws", func(c *fiber.Ctx) error {
				if !websocket.IsWebSocketUpgrade(c) {
					return fiber.ErrUpgradeRequired
				}
				return c.Next()
			}).
				Get("/ws/events", websocket.New(server.WS)).
				Put("/put/events", server.putEvent)
			return app.Listen(fmt.Sprintf(":%d", conf.port))
		})
}
