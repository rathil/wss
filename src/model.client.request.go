package src

type modelClientRequest struct {
	Alive     *bool     `json:"alive"`
	Subscribe *[]string `json:"subscribe"`
}

func (a modelClientRequest) Process(s iSubscriber) error {
	switch {
	case a.Alive != nil:
		return s.alive()
	case a.Subscribe != nil:
		return s.subscribe(*a.Subscribe)
	}
	return nil
}
