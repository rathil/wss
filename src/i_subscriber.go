package src

type iSubscriber interface {
	alive() error
	subscribe(keys []string) error
	unsubscribe()
	end() <-chan error
}
