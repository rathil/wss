package src

import (
	"sync"
	"time"
)

func modelSubscriptionGet(
	conf config,
) *modelSubscription {
	model := &modelSubscription{
		conf:    conf,
		clients: map[listener]*modelClient{},
		events:  map[string]map[listener]bool{},
	}
	go model.gc()
	return model
}

type listener = chan []byte
type modelSubscription struct {
	conf    config
	lock    sync.RWMutex
	clients map[listener]*modelClient
	events  map[string]map[listener]bool
}

func (a *modelSubscription) gc() {
	for {
		time.Sleep(a.conf.connectionGc)
		t := time.Now().Add(-1 * a.conf.connectionTimeout)
		endClient := make([]*modelClient, 0, len(a.clients))
		a.lock.Lock()
		for _, client := range a.clients {
			if !client.isAlive(t) {
				endClient = append(endClient, client)
			}
		}
		a.lock.Unlock()
		for _, client := range endClient {
			client.unsubscribe()
		}
	}
}

// Create new subscriber for new connect
func (a *modelSubscription) subscriberFactory(list listener) iSubscriber {
	client := &modelClient{
		subscription: a,
		active:       time.Now(),
		eventIds:     map[string]bool{},
		list:         list,
		chEnd:        make(chan error, 1),
	}
	a.lock.Lock()
	a.clients[list] = client
	a.lock.Unlock()
	return client
}

// Subscribe client on events
func (a *modelSubscription) subscribe(client *modelClient, eventIds map[string]bool) {
	oldKeys := map[string]bool{}
	for key := range client.eventIds {
		if ok := eventIds[key]; !ok {
			oldKeys[key] = true
		}
	}
	newKeys := map[string]bool{}
	for key := range eventIds {
		if ok := client.eventIds[key]; !ok {
			newKeys[key] = true
		}
	}

	a.lock.Lock()
	a.unsubscribeEventsByKeysUnsafe(oldKeys, client.list)
	for key := range newKeys {
		if _, ok := a.events[key]; !ok {
			a.events[key] = map[listener]bool{}
		}
		a.events[key][client.list] = true
	}
	a.lock.Unlock()
}

// Unsubscribe client from events
func (a *modelSubscription) unsubscribe(client *modelClient) {
	a.lock.Lock()
	a.unsubscribeEventsByKeysUnsafe(client.eventIds, client.list)
	delete(a.clients, client.list)
	a.lock.Unlock()
}

// Unsafe unsubscribe from events (internal usage)
func (a *modelSubscription) unsubscribeEventsByKeysUnsafe(keys map[string]bool, list listener) {
	for key := range keys {
		if _, ok := a.events[key]; ok {
			delete(a.events[key], list)
			if len(a.events[key]) == 0 {
				delete(a.events, key)
			}
		}
	}
}

// Put event to clients
func (a *modelSubscription) putEvent(key string, data []byte) error {
	a.lock.RLock()
	if list, ok := a.events[key]; ok {
		for item := range list {
			item <- data
		}
	}
	a.lock.RUnlock()
	return nil
}
