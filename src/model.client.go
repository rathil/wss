package src

import (
	"io"
	"sync"
	"time"
)

type modelClient struct {
	lock         sync.Mutex
	subscription *modelSubscription
	active       time.Time
	list         listener
	eventIds     map[string]bool
	chEnd        chan error
}

func (a *modelClient) isAlive(t time.Time) bool {
	a.lock.Lock()
	defer a.lock.Unlock()
	return a.active.After(t)
}

func (a *modelClient) alive() error {
	a.lock.Lock()
	a.active = time.Now()
	a.lock.Unlock()
	return nil
}

func (a *modelClient) subscribe(keys []string) error {
	keyMap := map[string]bool{}
	for _, key := range keys {
		keyMap[key] = true
	}
	a.lock.Lock()
	a.subscription.subscribe(a, keyMap)
	a.eventIds = keyMap
	a.lock.Unlock()
	return nil
}

func (a *modelClient) unsubscribe() {
	a.lock.Lock()
	a.subscription.unsubscribe(a)
	a.chEnd <- io.EOF
	a.lock.Unlock()
}

func (a *modelClient) end() <-chan error {
	return a.chEnd
}
