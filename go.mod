module gitlab.com/rathil/wss

go 1.14

require (
	github.com/gofiber/fiber/v2 v2.0.3
	github.com/gofiber/websocket/v2 v2.0.1
	github.com/urfave/cli/v2 v2.2.0
	gitlab.com/rathil/rdi.git v1.0.0
)
