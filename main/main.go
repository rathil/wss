package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/rathil/wss/src"
	"log"
	"os"
	"time"
)

func main() {
	if err := (&cli.App{
		Name:    "WS server",
		Version: "v0.0.0",
		Commands: []*cli.Command{
			{
				Name:   "run",
				Usage:  "Run service",
				Action: src.CmdRun,
				Flags: []cli.Flag{
					&cli.DurationFlag{
						Name:    "connection-gc",
						EnvVars: []string{"CONNECTION_GC"},
						Value:   30 * time.Second,
					},
					&cli.DurationFlag{
						Name:    "connection-timeout",
						EnvVars: []string{"CONNECTION_TIMEOUT"},
						Value:   30 * time.Second,
					},
					&cli.IntFlag{
						Name:    "port",
						EnvVars: []string{"PORT"},
						Value:   3000,
					},
				},
			},
		},
		EnableBashCompletion: true,
		//Compiled:             tool.TimeBuild(),
		Authors: []*cli.Author{{
			Name:  "Ievgenii Kyrychenko",
			Email: "i.kyrychenko@nextpage.agency",
		}},
		Copyright: fmt.Sprintf("© 2020-%d nextpage.com.ua all rights reserved.", time.Now().Year()),
	}).Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
